﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("A3:Future Value Calculator");
			Console.WriteLine("Author: Juan Abreut");
			Console.WriteLine("Display short assignment requirements");
			Console.WriteLine("Display my name as author");
			Console.WriteLine("Display current date/time");
			Console.WriteLine("Must perform and display each mathematical operation\n");
			
	
	DateTime localDate = DateTime.Now;
			decimal initBalance = 0.0M;
			int years = 0;
			double interestRate = 0.0;
			decimal deposit = 0.0M;
			string testInput = " ";
			int counter = 0;
			Console.WriteLine("\nNow: " + localDate + "\n");
			
			while(true)
			{
				if(counter == 0)
				{
					Console.Write("Starting balance: ");
					testInput = Console.ReadLine();
					if (decimal.TryParse(testInput, out initBalance))
					{
						initBalance = (decimal)Convert.ToDouble(testInput);
						testInput = " ";
						counter++;
					}
				}
				if(counter == 1)
				{
					Console.Write("\nTerm (years): ");
					testInput = Console.ReadLine();
					if (int.TryParse(testInput, out years))
					{
						years = Convert.ToInt32(testInput);
						testInput = " ";
						counter++;
					}
				}
				if(counter == 2)
				{
					Console.Write("\nInterest rate: ");
					testInput = Console.ReadLine();
					if (double.TryParse(testInput, out interestRate))
					{
						interestRate = Convert.ToDouble(testInput);
						testInput = " ";
						counter++;
					}
				}
				if(counter == 3)
				{
					Console.Write("\nDeposit (monthly): ");
					testInput = Console.ReadLine();
					if (decimal.TryParse(testInput, out deposit))
					{
						deposit = (decimal)Convert.ToDouble(testInput);
						break;
					}
				}
				if(counter == 0)
				{
					Console.WriteLine("Starting Balance must be numeric");
					continue;
				}
				else if(counter == 1)
				{
					Console.WriteLine("Term must be integer data type");
					continue;
				}
				else if(counter == 2)
				{
					Console.WriteLine("Interest rate must be numeric");
					continue;
				}
				else 
				{
					Console.WriteLine("Monthly deposit must be numeric");
					continue;
				}	
			}
			Console.WriteLine("\n*** Future Value ***");
			Future(initBalance, interestRate, years, deposit);
			Console.Write("\nPress any key to exit!");
			Console.ReadKey();
			
		}
		static void Future(decimal initBalance, double interestRate, int years, decimal deposit)
		{
			interestRate = interestRate/1200;
			years = years * 12;
			Console.WriteLine(String.Format("{0:C}",initBalance*(decimal)Math.Pow((1 + interestRate),years) + (deposit*(decimal)(Math.Pow(1+interestRate, years) - 1)/(decimal)interestRate))); 
		}
	}
}
