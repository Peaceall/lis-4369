using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DateTime localDate = DateTime.Now;
			double num1 = 0.0;  // Double to store have a more accurate calculation than float
			double num2 = 0.0;	
			int choice = 0; 	//User input variable
			
			
			Console.WriteLine("A2: Simple Calculator");
			Console.WriteLine("Author: Juan Abreut");
			Console.WriteLine("Now: " + localDate + "\n");
			Console.WriteLine("Display short assignment requirements");
			Console.WriteLine("Display my name as author");
			Console.WriteLine("Display current date/time");
			Console.WriteLine("Must perform and display each mathematical operation\n");
			
			Console.Write("num1: ");
			num1 = Convert.ToDouble(Console.ReadLine());
			
			Console.Write("num2: ");
			num2 = Convert.ToDouble(Console.ReadLine());
			
			Console.WriteLine("\n1: Addition");
			Console.WriteLine("2: Subtraction");
			Console.WriteLine("3: Multiplication");
			Console.WriteLine("4: Division");
			Console.WriteLine("5: Exponentation");
				
			
				Console.Write("\nChoose a mathematical operation: ");
				choice = Convert.ToInt32(Console.ReadLine());

				switch (choice)
				{
					case 1:
						Addition(num1,num2);
						break;
						
					case 2:
						Subtraction(num1,num2);
						break;
						
					case 3:
						Multiplication(num1,num2);
						break;
						
					case 4:
						if(num2 == 0)
						{
							Console.WriteLine("\nError, division by zero attempted aborting ");
							break;
						}
						Division(num1,num2);
						break;
					case 5:
						Exponentation(num1,num2);
						break;
						
					default:
						Console.WriteLine("\nError, please input an integer between 1-5");
						break;
				}
			
			Console.Write("\nPress any key to exit");
			Console.ReadKey(true);
		
		}
		public static void Addition(double num1, double num2)
		{
			Console.Write("\nResult of Addition Operation: " );
			Console.WriteLine(num1 + num2);
		}
		public static void Subtraction(double num1, double num2)
		{
			Console.Write("\nResult of Subtraction Operation: ");
			Console.WriteLine(num1 - num2);
		}
		public static void Multiplication(double num1, double num2)
		{
			Console.Write("\nResult of Multiplication Operation: ");
			Console.WriteLine(num1 * num2);
		}
		public static void Division(double num1, double num2)
		{
			Console.Write("\nResult of Division Operation: ");
			Console.WriteLine(num1 / num2);
		}
		public static void Exponentation(double num1, double num2)
		{
			Console.Write("\nResult of Exponentiation Operation: ");
			Console.WriteLine(Math.Pow(num1,num2));
		}
    }
}
