# LIS4369 - Extensible Enterprise Soulutions

## Juan Abreut

### Assignments: 
 
* [In this repo: A1 README.md](a1)

    * Install .NET Core

    * Create hwapp application

    * Create aspnetcoreapp application

    * Provide screenshots of installation

    * Create Bitbucket repo

    * Complete Bitbucket tutorials (see bottom)

    * Provide Git command descriptions

* [In this repo: A2 README.md](a2)

    * Reverse Engineer Console Application
	
	* Implement basic Flow Control
	
	* Provide calculated out based on user entered values
	
    
* [In this repo: A3 README.md](a3)

    * Future value calculator
	
	* Implement error handling and basic data validation
	
	* Implement basic looping control
	
* [In this repo: P1 README.md](p1)

    * Reverse Engineer Console Application
	
	* Implement error checking
	
	* Construct Object Oriented Room Class
	
* [In this repo: A4 README.md](a4)

    * Reverse Engineer Console Application
	* Implement data validation
	* Provide calculated student class which is derived from person base class
	
* [In this repo: P2 README.md](p2)

    * Implelment LINQ in program
	* Follow Microsoft tutorial
	* Employ search funcitonality 
	
	
* [BitBucketStationLocations Tutorial](https://bitbucket.org/Peaceall/bitbucketstationlocations)
 
* [Myteamquotes Tutorial](https://bitbucket.org/Peaceall/myteamquotes)