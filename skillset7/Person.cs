using System;
namespace a4
{
public class Person
	{
		protected string fname;
		protected string lname;
		protected int age;
		
		public Person()
		{
			fname = "John";
			lname = "Doe";
			age = 0;
			Console.WriteLine("Creating default person object");
		}
		
		public Person (string fn, string ln, int a)
		{
			fname = fn;
			lname = ln;
			age = a;
			Console.WriteLine("Creating person object from user input");
		}
		
		public void SetFname(string nm)
		{
			fname = nm;
		}
		
		public void SetLname(string nm)
		{
			lname = nm;
		}
		
		public void SetAge(int a)
		{
			age = a;
		}
		
		public string GetFname()
		{
			return fname;
		}
		
		public string GetLname()
		{
			return lname;
		}
		
		public int GetAge()
		{
			return age;
		}
		
		public virtual string GetObjectInfo()
		{
			return fname +" " + lname + " " + age.ToString();
		}
	}		
}