using System;
namespace a4
{
public class Student: Person
	{
		private string college;
		private string major;
		private double gpa;
		
		public Student()
		{
			college = "test";
			major = "value";
			gpa = 0.0;
			Console.WriteLine("Creating default student object");
		}
		
		public Student (string fn ="", string ln="", int a=0, string c, string m, double g: base(fn,ln,a))
		{
			college = c;
			major= m;
			gpa = g;
			Console.WriteLine("Creating student object from user input");
		}
		
		public string GetName()
		{
			return fname + " " + lname;
		}
		public void SetCollege(string c)
		{
			college = c;
		}
		
		public void SetMajor(string m)
		{
			major = m;
		}
		
		public void SetGpa(double g)
		{
			gpa = g;
		}
		
		public string GetCollege()
		{
			return college;
		}
		
		public string GetMajor()
		{
			return major;
		}
		
		public int GetGpa()
		{
			return gpa;
		}
		
		public override string GetObjectInfo()
		{
			return base.GetObjectInfo() +" in the college of " + this.college + ",  majoring in " + this.major + ", and has a " + this.gpa + " gpa";
		}
	}		
}
