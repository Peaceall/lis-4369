﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
		public static void Main(string[] args)
        {
		double num1 = 0;
		Console.Write("Please Enter a number: ");
		while(!double.TryParse(Console.ReadLine(), out num1))
			{
				Console.WriteLine("num must be double : ");
			}
		
		Console.WriteLine("Call PassByVal()");
		PassByVal(num1);
		Console.WriteLine("Inside main num1 = " + num1 + '\n');
		Console.WriteLine("Call PassByRef()");
		PassByRef(ref num1);
		Console.WriteLine("Inside main num1 = " + num1 + '\n');
		}
	
	
	public static void PassByVal(double a)
	{
		Console.WriteLine("Inside PassByVal()");
		a = a+5;
		Console.WriteLine("Added 5 to number pass by reference = " + a);
	}
	
	public static void PassByRef(ref double a)
	{
		Console.WriteLine("Inside PassByReference()");
		a = a+5;
		Console.WriteLine("Added 5 to number pass by reference = " + a);
	}
	}
}
