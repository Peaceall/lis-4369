using System;
namespace ConsoleApplication
{
public class Room
	{
		private string type;
		private double length;
		private double width;
		private double height;
		
		public void SetType(string t)
		{
			type = t;
		}
		
		public void SetLength(double l)
		{
			length = l;
		}
		
		public void SetWidth(double w)
		{
			width = w;
		}
		
		public void SetHeight(double h)
		{
			height = h;
		}
		
		public string GetType()
		{
			return type;
		}
		
		public double GetLength()
		{
			return length;
		}
		
		public double GetWidth()
		{
			return width;
		}
		
		public double GetHeight()
		{
			return height;
		}
		
		public double GetArea()
		{
			return (length * width);
		}
		
		public double GetVolume()
		{
			return (length * width * height);
		}
		
		public Room()
		{
			Console.WriteLine("Creating Room object with default parameters");
			type = "Default";
			length = 10;
			width = 10;
			height = 10;
		}
		
		public Room (string t, double l, double w, double h)
		{
			Console.WriteLine("Creating parametrized constructor (accepts two arguments)");
			type = t;
			length = l;
			width = w;
			height = h;
		}
	}		
}