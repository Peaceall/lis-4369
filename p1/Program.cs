﻿using System;
namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
			DateTime localDate = DateTime.Now;
			string type = " ";
			double length = 0.0;
			double width = 0.0;
			double height = 0.0;
			string testInput = " ";
			int counter = 0;
			int test;
            Console.WriteLine("SkillSet 7");
			Console.WriteLine("Author: Juan Abreut");
			Console.WriteLine("Now: " + localDate + "\n");
			
			Room Room1 = new Room();
			Console.Write("Room Type: " + Room1.GetType() + '\n');
			Console.Write("Room Length: " + Room1.GetLength() + '\n');
			Console.Write("Room Width: " + Room1.GetWidth() + '\n');
			Console.Write("Room Height: " + Room1.GetHeight() + '\n');
			Console.Write("Room Area: " + Math.Round(Room1.GetArea(),2).ToString("0.00") + " sq ft" + '\n');
			Console.Write("Room Volume: " + Math.Round(Room1.GetVolume(), 2).ToString("0.00") + " cu ft" + '\n');
			Console.Write("Room Volume: " + Math.Round(Room1.GetVolume()/27, 2).ToString("0.00") + " cu yd" + '\n');
			
			Console.WriteLine("\nModify default constructor object's data member value");
			Console.WriteLine("Using setter getter methods");
			while(true)
			{
			if (counter == 0)
			{
				Console.Write("Room Type: ");
				testInput = Console.ReadLine();
				if (int.TryParse(testInput, out test))
				{
					Console.WriteLine("Please input a string");
					continue;
				}
				else
				{
					type = testInput;
					testInput = " ";
					counter++;
				}
			}
			if (counter == 1)
			{
				Console.Write("Room Length: ");
				testInput = Console.ReadLine();
				if (double.TryParse(testInput, out length) && Convert.ToDouble(testInput) >= 0)
				{
					length = Convert.ToDouble(testInput);
					testInput = " ";
					counter++;
				}
				else
				{
					Console.WriteLine("Please input a positive number");
					continue;
				}
			}
			if (counter == 2 )
			{
				Console.Write("Room Width: ");
				testInput = Console.ReadLine();
				if (double.TryParse(testInput, out width) && Convert.ToDouble(testInput) >= 0)
				{
					width = Convert.ToDouble(testInput);
					testInput = " ";
					counter++;
				}
				else
				{
					Console.WriteLine("Please input a positive number");
					continue;
				}
			}
			if (counter == 3)
			{
				Console.Write("Room Height: ");
				testInput = Console.ReadLine();
				if (double.TryParse(testInput, out height) && Convert.ToDouble(testInput) >= 0)
				{
					height = Convert.ToDouble(testInput);
					testInput = " ";
					break;
				}
				else
				{
					Console.WriteLine("Please input a positive number");
					continue;
				}
			}
			};
			Room1.SetType(type);
			Room1.SetLength(length);
			Room1.SetWidth(width);
			Room1.SetHeight(height);
			counter = 0;
			
			Console.WriteLine("\nDisplaying objects new member data value");
			Console.Write("Room Type: " + Room1.GetType() + '\n');
			Console.Write("Room Length: " + Room1.GetLength() + '\n');
			Console.Write("Room Width: " + Room1.GetWidth() + '\n');
			Console.Write("Room Height: " + Room1.GetHeight() + '\n');
			Console.Write("Room Area: " + Math.Round(Room1.GetArea(),2).ToString("0.00") + " sq ft" + '\n');
			Console.Write("Room Volume: " + Math.Round(Room1.GetVolume(), 2).ToString("0.00") + " cu ft" + '\n');
			Console.Write("Room Volume: " + Math.Round(Room1.GetVolume()/27, 2).ToString("0.00") + " cu yd" + '\n');
			
						while(true)
			{
			if (counter == 0)
			{
				Console.Write("Room Type: ");
				testInput = Console.ReadLine();
				if (int.TryParse(testInput, out test))
				{
					Console.WriteLine("Please input a string");
					continue;
				}
				else
				{
					type = testInput;
					testInput = " ";
					counter++;
				}
			}
			if (counter == 1)
			{
				Console.Write("Room Length: ");
				testInput = Console.ReadLine();
				if (double.TryParse(testInput, out length) && Convert.ToDouble(testInput) >= 0)
				{
					length = Convert.ToDouble(testInput);
					testInput = " ";
					counter++;
				}
				else
				{
					Console.WriteLine("Please input a positive number");
					continue;
				}
			}
			if (counter == 2)
			{
				Console.Write("Room Width: ");
				testInput = Console.ReadLine();
				if (double.TryParse(testInput, out width) && Convert.ToDouble(testInput) >= 0)
				{
					width = Convert.ToDouble(testInput);
					testInput = " ";
					counter++;
				}
				else
				{
					Console.WriteLine("Please input a positive number");
					continue;
				}
			}
			if (counter == 3)
			{
				Console.Write("Room Height: ");
				testInput = Console.ReadLine();
				if (double.TryParse(testInput, out height) && Convert.ToDouble(testInput) >= 0)
				{
					height = Convert.ToDouble(testInput);
					testInput = " ";
					break;
				}
				else
				{
					Console.WriteLine("Please input a positive number");
					continue;
				}
			}
			};
			
			Room Room2 = new Room(type, length, width, height);
			
			Console.WriteLine("\nShowing Room values from parametrized constructor");
			Console.Write("Room Type: " + Room2.GetType() + '\n');
			Console.Write("Room Length: " + Room2.GetLength() + '\n');
			Console.Write("Room Width: " + Room2.GetWidth() + '\n');
			Console.Write("Room Height: " + Room2.GetHeight() + '\n');
			Console.Write("Room Area: " + Math.Round(Room2.GetArea(),2) + " sq ft" + '\n');
			Console.Write("Room Volume: " + Math.Round(Room2.GetVolume(), 2) + " cu ft" + '\n');
			Console.Write("Room Volume: " + Math.Round(Room2.GetVolume()/27, 2) + " cu yd" + '\n');
			Console.Write("\nPress any key to exit");
			Console.ReadKey(true);
        }
    }
}