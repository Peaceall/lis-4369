﻿using System;
namespace a4
{
    public class Program
    {
        public static void Main(string[] args)
        {
			DateTime localDate = DateTime.Now;
			string fname;
			string lname;
			string college;
			string major;
			double gpa;
			int age;
			
            Console.WriteLine("Assignment 4");
			Console.WriteLine("Author: Juan Abreut");
			Console.WriteLine("Now: " + localDate + "\n");
			
			Person Person1 = new Person();
			Console.Write("First Name: " + Person1.GetFname() + '\n');
			Console.Write("Last Name: " + Person1.GetLname() + '\n');
			Console.Write("Age: " + Person1.GetAge() + '\n');
			
			Console.WriteLine("\nModify default constructor object's data member value");
			Console.WriteLine("Using setter getter methods");
			Console.Write("First Name: ");
			fname = Console.ReadLine();
			Console.Write("Last Name: ");
			lname = Console.ReadLine();
			Console.Write("Age: ");
			while(!int.TryParse(Console.ReadLine(), out age))
			{
				Console.WriteLine("Age must be integer");
			}
			
			Person1.SetFname(fname);
			Person1.SetLname(lname);
			Person1.SetAge(age);
			
			Console.WriteLine("\nDisplaying objects new member data value");
			Console.WriteLine("First Name: " + Person1.GetFname());
			Console.WriteLine("Last Name: " + Person1.GetLname());
			Console.Write("Age: " + Person1.GetAge() + '\n');
			
			Console.WriteLine("\nCall parametrized constructor (accepts three arguments)");
			Console.Write("First Name: ");
			fname = Console.ReadLine();
			Console.Write("Last Name: ");
			lname = Console.ReadLine();
			Console.Write("Age: ");
			while(!int.TryParse(Console.ReadLine(), out age))
			{
				Console.WriteLine("Age must be integer");
			}
			Person Person2 = new Person(fname, lname, age);
			
			Console.WriteLine("First Name: " + Person2.GetFname());
			Console.WriteLine("Last Name: " + Person2.GetLname());
			Console.WriteLine("Age: " + Person2.GetAge() + '\n');
			
			Student student1 = new Student();
			
			Console.WriteLine("First Name: " + student1.GetFname());
			Console.WriteLine("Last Name: " + student1.GetLname());
			Console.Write("Age: " + student1.GetAge() + '\n');
			Console.Write("Demonstrating polymorphism\n");
			Console.Write("First Name: ");
			fname = Console.ReadLine();
			Console.Write("Last Name: ");
			lname = Console.ReadLine();
			Console.Write("Age: ");
			while(!int.TryParse(Console.ReadLine(), out age))
			{
				Console.WriteLine("Age must be integer");
			}
			Console.Write("College: ");
			college = Console.ReadLine();
			Console.Write("Major: ");
			major = Console.ReadLine();
			Console.Write("GPA: ");
			while(!double.TryParse(Console.ReadLine(), out gpa))
			{
				Console.WriteLine("GPA must be a double");
			}
			Student student2 = new Student(fname, lname, age, college, major, gpa);
			Console.WriteLine();
			Console.Write("person 2 get object info\n");
			Console.WriteLine(Person2.GetObjectInfo());
			Console.Write("student 2 get object info\n");
			Console.WriteLine(student2.GetObjectInfo());
			Console.Write("\nPress any key to exit");
			Console.ReadKey(true);
        }
    }
}

		